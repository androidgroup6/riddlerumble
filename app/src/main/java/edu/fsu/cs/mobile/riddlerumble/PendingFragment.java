package edu.fsu.cs.mobile.riddlerumble;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class PendingFragment extends Fragment {

    ListView list;
    public static ArrayAdapter<String> arrayAdapter;

    public static String user;

    Riddle current;

    public static String riddle;
    public static String answer;
    public static String hint1;
    public static String hint2;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public PendingFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static PendingFragment newInstance() {
        PendingFragment fragment = new PendingFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pending, container, false);

        list = (ListView) view.findViewById(R.id.listView_pending_riddles);

        arrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, MainActivity.pendRiddles);

        list.setAdapter(arrayAdapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view1, int i, long l){
                user = arrayAdapter.getItem(i);

                for(int j = 0; j < MainActivity.pendRiddles.size(); j++) {
                    if(MainActivity.pendRiddles.get(j).equals(user))
                        current = MainActivity.totalriddles.get(j);
                }
                riddle=current.getRiddle();
                answer =current.getSolution();
                hint1=current.getHint1();
                hint2= current.getHint2();
                Fragment fragment = ResponseFragment.newInstance();
                MainActivity.getFragManager().beginTransaction().replace(R.id.flContent, fragment).addToBackStack(MainActivity.getToolbarName()).commit();
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
