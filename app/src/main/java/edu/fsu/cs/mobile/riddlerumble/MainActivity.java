package edu.fsu.cs.mobile.riddlerumble;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        MakeRiddleFragment.OnFragmentInteractionListener,
        FriendsFragment.OnFragmentInteractionListener,
        LeaderboardFragment.OnFragmentInteractionListener,
        PendingFragment.OnFragmentInteractionListener,
        ResponseFragment.OnFragmentInteractionListener {

    static final String TAG = "MainActivity";
    static Toolbar toolbar;
    static private FragmentManager fragmentManager;
    NavigationView navigationView;

    int gameScore;
    View header;

    public static Firebase ref;

    public static ArrayList<String> pendRiddles;
    public static ArrayList<Riddle> totalriddles;
    String testString;

    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Firebase auth testing
        firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        testString = LoginActivity.getUsername();
        if (user != null) {
            String email = user.getEmail();
            Log.i(TAG, "Firebase auth testing passed");
        }
        else{
            Log.d(TAG, "Firebase auth testing passed");
        }

        // Firebase db testing
        Firebase.setAndroidContext(this);
        ref = new Firebase(Config.FIREBASE_URL);

        totalriddles= new ArrayList<Riddle>();
        pendRiddles=new ArrayList<String>();
        //get database child names
        ref.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                Log.i("Count " ,""+dataSnapshot.getChildrenCount());


                if (pendRiddles != null) {
                    pendRiddles.clear();
                }

                for (com.firebase.client.DataSnapshot child: dataSnapshot.getChildren()) {
                    Log.i("MainActivity", child.getKey());
                    pendRiddles.add(child.getKey());
                   Riddle cur= child.getValue(Riddle.class);

                    totalriddles.add(child.getValue(Riddle.class));

                    Log.i(TAG, "Riddle: " + cur.getRiddle());
                }

                PendingFragment.arrayAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }

        });



        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        renameToolbar(getString(R.string.nav_pending_riddles));
        Fragment fragment = PendingFragment.newInstance();

        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).addToBackStack(getString(R.string.nav_make_riddle)).commit();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        header = navigationView.getHeaderView(0);
        TextView username = (TextView) header.findViewById(R.id.usernameTextView);
        username.setText("Hello, " + LoginActivity.getUsername() + "!");
        gameScore = 0;
        navigationView.getMenu().getItem(0).setChecked(true);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();





    }

    public static FragmentManager getFragManager() {
        return fragmentManager;
    }

    public static String getToolbarName() {
        return toolbar.getTitle().toString();
    }

    public void renameToolbar(String title) {
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }

        int count = fragmentManager.getBackStackEntryCount();
        if (count != 1) {
            fragmentManager.popBackStack();
            renameToolbar(fragmentManager.getBackStackEntryAt(count - 1).getName());
            for(int i = 0; i < navigationView.getMenu().size(); i++)
                if(navigationView.getMenu().getItem(i).getTitle().equals(fragmentManager.getBackStackEntryAt(count - 1).getName())){
                    navigationView.getMenu().getItem(i).setChecked(true);
                } else {
                    navigationView.getMenu().getItem(i).setChecked(false);
                }
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment = null;
        String toolbarTitle = getToolbarName();

        if (id == R.id.nav_friends) {
            renameToolbar(getString(R.string.nav_friends));
            fragment = FriendsFragment.newInstance();
        } else if (id == R.id.nav_make_riddle) {
            renameToolbar(getString(R.string.nav_make_riddle));
            fragment = MakeRiddleFragment.newInstance();
        } else if (id == R.id.nav_pending_riddles) {
            renameToolbar(getString(R.string.nav_pending_riddles));
            fragment = PendingFragment.newInstance();
        }  else if (id == R.id.nav_leaderboard) {
            renameToolbar(getString(R.string.nav_leaderboard));
            fragment = LeaderboardFragment.newInstance();
        } else if (id == R.id.nav_logout) {
            Intent mIntent = new Intent(this, LoginActivity.class);
            startActivity(mIntent);
        }

        if(fragment != null)
            fragmentManager.beginTransaction().replace(R.id.flContent, fragment).addToBackStack(toolbarTitle).commit();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public void displayHint1(View view){
        gameScore -= 10;
        TextView hint = (TextView) view;
        hint.setText("Hint 1: " + PendingFragment.hint1);
        updateScore();
    }

    public void displayHint2(View view){
        gameScore -= 10;
        TextView hint = (TextView) view;
        hint.setText("Hint 2: " + PendingFragment.hint2);
        updateScore();
    }

   public void checkSolution(View view){
       if(PendingFragment.answer.toLowerCase().equals(ResponseFragment.solution.getText().toString().toLowerCase()))
       {
           gameScore += 100;
           Toast.makeText(this, "Congrats! You solved it!", Toast.LENGTH_SHORT).show();
           updateScore();
       }
       else {
           gameScore -= 25;
           Toast.makeText(this, "Wrong", Toast.LENGTH_SHORT).show();
           updateScore();
       }
   }

    private void updateScore(){
        TextView score = (TextView) header.findViewById(R.id.textView_score);
        score.setText("Score:\t" + gameScore);
    }

    public void submitRiddle(View view){
        EditText riddleText = (EditText) findViewById(R.id.editText_riddle);
        EditText solutionText = (EditText) findViewById(R.id.editText_solution);
        EditText hint1Text = (EditText) findViewById(R.id.editText_hint1);
        EditText hint2Text = (EditText) findViewById(R.id.editText_hint2);

        if((riddleText.length() == 0) || (solutionText.length() == 0)/* || (hint1Text.length() == 0) || (hint2Text.length() == 0)*/) {
            Toast.makeText(this, "All fields must have text", Toast.LENGTH_SHORT).show();
            return;
        }

        Riddle riddle = new Riddle();
        riddle.setRiddle(riddleText.getText().toString());
        riddle.setSolution(solutionText.getText().toString());
        riddle.setHint1(hint1Text.getText().toString());
        riddle.setHint2(hint2Text.getText().toString());

        ref.child(testString).setValue(riddle);

        Toast.makeText(this, "Your riddle has been posted!", Toast.LENGTH_SHORT).show();
        riddleText.getText().clear();
        solutionText.getText().clear();
        hint1Text.getText().clear();
        hint2Text.getText().clear();
    }
}
