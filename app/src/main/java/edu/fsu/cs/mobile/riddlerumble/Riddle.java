package edu.fsu.cs.mobile.riddlerumble;

/**
 * Created by kurti on 12/1/2016.
 */

public class Riddle {
   // private String username;
    private String riddle;
    private String solution;
    private String hint1;
    private String hint2;

    public Riddle() {
      // Required for Firebase
    }
/*
    public String getUsername() {return username;}
    public void setUsername(String username) {
        this.username = username;
    }
*/
    public String getRiddle() {
        return riddle;
    }
    public void setRiddle(String riddle) {
        this.riddle = riddle;
    }

    public String getSolution() {
        return solution;
    }
    public void setSolution(String solution) {
        this.solution = solution;
    }

    public String getHint1() {
        return hint1;
    }
    public void setHint1(String hint1) {
        this.hint1 = hint1;
    }

    public String getHint2() {
        return hint2;
    }
    public void setHint2(String hint2) {
        this.hint2 = hint2;
    }
}
