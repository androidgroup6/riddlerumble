package edu.fsu.cs.mobile.riddlerumble;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.firebase.client.Firebase;

public class LoginActivity extends AppCompatActivity {

    static final String TAG = "LoginActivity";
    Button Login;

    TextView PromptText;

    EditText Username;
    EditText Password;
    EditText ConfirmPassword;

    private static String username;
    private String password;

    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Login = (Button) findViewById(R.id.login_button);
        PromptText = (TextView) findViewById(R.id.signUpText);

        Username = (EditText) findViewById(R.id.username);
        Password = (EditText) findViewById(R.id.password);
        ConfirmPassword = (EditText) findViewById(R.id.confirmPswd);

        firebaseAuth = FirebaseAuth.getInstance();
    }

    public static String getUsername() {
        return username.split("@")[0];
    }

    public void myLoginHandler(View v) {
        if (v == Login) {
            if (Login.getText().toString().equals("Log In"))
                pressedLogin();
            else
                pressedSignUp();
        } else if (v == PromptText)
            switchForm();
    }

    private void pressedLogin() {
        username = Username.getText().toString().trim();
        password = Password.getText().toString().trim();

        Log.d(TAG, "In pressedLogin()");

        ConfirmPassword.getText().clear();
        if (checkFields()) {
            firebaseAuth.signInWithEmailAndPassword(username, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(myIntent);
                            } else {
                                Toast.makeText(getApplicationContext(), "Login failed", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        } else
            Toast.makeText(this, "Invalid Email/Password", Toast.LENGTH_SHORT).show();
    }

    private void pressedSignUp() {
        username = Username.getText().toString().trim();
        password = Password.getText().toString().trim();

        if (checkFields()) {
            //creating a new user
            firebaseAuth.createUserWithEmailAndPassword(username, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            //checking if success
                            if (task.isSuccessful()) {
                                Toast.makeText(getApplicationContext(), "Sign up success", Toast.LENGTH_SHORT).show();
                                Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(myIntent);
                            } else {
                                Toast.makeText(getApplicationContext(), "Sign up failure", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
    }

    private void switchForm() {
        if (Login.getText().toString().equals("Log In")) {
            ConfirmPassword.setVisibility(View.VISIBLE);
            Login.setText(getString(R.string.button_signup));
            PromptText.setText(getString(R.string.login_prompt2));
        } else {
            ConfirmPassword.setVisibility(View.GONE);
            Login.setText(getString(R.string.button_login));
            PromptText.setText(getString(R.string.login_prompt));
        }
    }

    private boolean checkFields() {//checks if a valid email or password has been entered
        boolean check = true;
        String message = "Invalid ";

        username = Username.getText().toString();
        if( (username.length() == 0) || (!android.util.Patterns.EMAIL_ADDRESS.matcher(username).matches())){
            message = message.concat("Email");
            check = false;
        }

        if (ConfirmPassword.getVisibility() == View.VISIBLE) {
            if (!Password.getText().toString().equals(ConfirmPassword.getText().toString())) {
                Password.getText().clear();
                ConfirmPassword.getText().clear();
                message = message.concat((message.equals("Invalid Email")) ? "/Password" : "Password");
                check = false;
            } else if (Password.getText().toString().length() == 0) {
                Password.getText().clear();
                ConfirmPassword.getText().clear();
                message = message.concat((message.equals("Invalid Email")) ? "/Password" : "Password");
                check = false;
            }
        } else if (Password.getText().toString().length() == 0) {
            message = message.concat((message.equals("Invalid Email")) ? "/Password" : "Password");
            check = false;
        }
        if (!check) {
            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        }
        return check;
    }

    @Override
    public void onBackPressed() {/*makes back button do nothing*/}
}